import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.BorderLayout;

/**
 * Décrivez votre classe CompteurV1 ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class CompteurV1 extends JFrame
{
    
private JPanel containerBoutons;
private JPanel containerAffiche;
private String title="";
private JButton boutonPlus;
private JButton boutonMoins;
private int nombre = 0;
JLabel label = new JLabel("nombre : " +nombre);

// Constructeur
public CompteurV1(String title) { 
this.title=title;
this.setTitle(title);
this.setSize(300, 150);
this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fermer le programme avec la fenêtre
this.setLocationRelativeTo(null); // Centrer la fenêtre sur l’écran
this.setVisible(true);
this.setLayout(new BorderLayout());

//Panels
containerBoutons=new JPanel();
containerAffiche=new JPanel();


//Attribution aux variables boutons l'instanciation des objets JButton
boutonPlus = new JButton("+");
boutonPlus.setPreferredSize(new Dimension(120, 40));
boutonMoins = new JButton("-");
boutonMoins.setPreferredSize(new Dimension(120, 40));

//Ajout des boutons au containerBoutons
containerBoutons.add(boutonPlus);
containerBoutons.add(boutonMoins);

//Ajout du label au containerAffiche
containerAffiche.add(label);


//Ajout des listeners
boutonPlus.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
        incrementeNombre();
    }
});

boutonMoins.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
        decrementeNombre();
    }
});


//Ajout du container a la fenetre
this.add(containerBoutons, BorderLayout.SOUTH);
this.add(containerAffiche, BorderLayout.NORTH);

}

public void incrementeNombre(){
      this.nombre++;
      label.setText("nombre : "+ nombre);
}
public void decrementeNombre(){
      this.nombre--;
      label.setText("nombre : "+ nombre);
}






















}
